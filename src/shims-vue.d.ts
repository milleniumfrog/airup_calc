declare module '*.vue' {
  import {defineComponent} from 'vue'
  type tmp = ReturnType<typeof defineComponent>;
  const abc: tmp;
  export default abc;
}
